// Write two ways of creating objects using functions that represent
// the items of the products on the products.jpg on the source directory

// Factory Function
// You code here ...
function createFood(name, info, cost) {
    return {
        name,
        info,
        cost
    };
}
const popular = createFood('Popular', 'meat, potato, garlik, lavash', '3$');
const retro = createFood('Retro', 'meat, potato, garlik, lavash', '2.8$');
const tradition = createFood('Tradition', 'meat, potato, garlik, lavash', '2$');
const trend = createFood('Trend', 'meat, potato, garlik, lavash', '2.5$');

console.log(popular);
console.log(retro);
console.log(tradition);
console.log(trend);


// Constructor Function
// You code here ...
function JunkFood(name, info, cost) {
    this.name = name,
    this.info = info,
    this.cost = cost
}

const twister = new JunkFood ('twister', 'chiken, salad, sauce, tomatos', '3.99$');
const chickenWings = new JunkFood ('chickenWings', 'chiken, salad, sauce, tomatos', '3.4$');
const bites = new JunkFood ('bites', 'chiken, salad, sauce, tomatos', '3$');
const friendsSet = new JunkFood ('friendsSet', 'chiken, salad, sauce, tomatos', '2$');

console.log(twister);
console.log(chickenWings);
console.log(bites);
console.log(friendsSet);
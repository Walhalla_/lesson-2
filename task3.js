// Write a function that takes two arrays (a and b) as arguments
// Create an object that has properties with keys 'a' and corresponding values 'b'
// Return the object

function myFunction(a, b) {
  // Your code here...
  var number = {};
  a.forEach((element, index) => {
    number[element] = b[index];
  });
  return number;
}

myFunction(['a', 'b', 'c'], [1, 2, 3]);
console.log(myFunction(['a', 'b', 'c'], [1, 2, 3]));
myFunction(['w', 'x', 'y', 'z'], [10, 9, 5, 2]);
console.log(myFunction(['w', 'x', 'y', 'z'], [10, 9, 5, 2]));
myFunction([1, 'b'], ['a', 2]);
console.log(myFunction([1, 'b'], ['a', 2]));